class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :first_name, :last_name

  validates_presence_of :first_name, :last_name
  validates :first_name, length: { maximum: 30 }
  validates :last_name, length: { maximum: 30 }
  
end
